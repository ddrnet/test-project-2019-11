package com.testcase.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Work.class)
public abstract class Work_ {

	public static volatile SingularAttribute<Work, String> name;
	public static volatile SingularAttribute<Work, Long> Emp;
	public static volatile SingularAttribute<Work, Long> id;

	public static final String NAME = "name";
	public static final String EMP = "Emp";
	public static final String ID = "id";

}

