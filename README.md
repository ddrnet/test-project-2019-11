# TEST REST SERVICE ON JAVA
## Структура
Созданный проект построен на основе 4х основных типов классов/интерфейсов:
1.  model - логическая структура создаваемых объектов/таблиц
2.  repository - интерфейс наследует JpaRepository предоставляя базовые методы взаиодействия с объектами бд, может быть дополнен собственными методами (к примеру на основе запросов)
3.  DAO - класс который обеспечивает логическую обработку парметров, построение обектов и вызывает необходимые методы из repository
4.  controller - класс обрабатывает входящие запросы к серверу и формирует соотвествующие ответы, вызывает методы из DAO после проверки принятых параметров в соответсвии с типо запроса и адреса обращения.
## Созданные классы - модели
1. Класс рабочего (таблица Employees)
```java
@Entity
@Table(name="Employees")
@EntityListeners(AuditingEntityListener.class)

//our model
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)  //Auto increment when new value inserted
    private Long id;

	@NotBlank
    private String name;

    @NotBlank
    private String position;

  
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date createdAt;

	@Column(columnDefinition = "BOOLEAN DEFAULT true", nullable = false)
    private Boolean isWorking = true;
	
	@Column(nullable = false)
    private Double salary;
..
}
```
2. Класс работы (таблица Works)
```java
@Entity
@Table(name="Works")
@EntityListeners(AuditingEntityListener.class)
public class Work {
	 	@Id
	    @GeneratedValue(strategy = GenerationType.AUTO)  //Auto increment when new value inserted
	    private Long id;

		@NotBlank
	    private String name;
		
		@Column(nullable = false)
		private Long Emp;
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}
..
}
```
## Примеры запросов к сервису (проверялось в Postman)
1. Таблица рабочих<br/> 
1.1. создание рабочего<br/> 
POST <br/> 
localhost:8080/company/employees<br/> 
BODY<br/> 
```json
{
    "name": "Yury",
    "position": "2", 
    "isWorking": true, 
    "salary": 4000.0
}
```
1.2. Получение всех записей из таблицы рабочих<br/> 
GET <br/> 
localhost:8080/company/employees<br/> 
1.3. Получение записи из таблицы рабочих по /id<br/> 
GET <br/> 
localhost:8080/company/employees/1<br/> 
1.4. Получение страницы записей из таблицы рабочих num - номер страницы (> 0), size - кол-во записей на странице (> 0)<br/> 
GET <br/> 
localhost:8080/company/employees/page?num=1&size=2<br/> 
1.5. Получение страницы записей из таблицы рабочих отсротированных по возростанию/ убываю по указанному (direction) параметру имя/зарплата (type)<br/> 
GET <br/> 
localhost:8080/company/employees/sort?type=name&direction=desc&num=1&size=2<br/> 
1.6. Удаление записи с указанным /id<br/> 
DELETE <br/> 
localhost:8080/company/employees/1<br/> 
1.7. Изменение данных записи из таблицы рабочих с указанным /id<br/> 
PUT <br/> 
localhost:8080/company/employees/1<br/> 
NEW BODY<br/> 
```json
{
    "name": "Dmitry",
    "position": "2",
    "isWorking": false,
    "salary": 2000.0
}
```
1.8. Поиск записей из таблицы рабочих с указанными полями (может быть больше, для примера только name)<br/> 
GET<br/> 
localhost:8080/company/employees/filter?name=Yury<br/> 
2. Таблица работ<br/> 
2.1. Создание новой работы<br/> 
POST <br/> 
localhost:8080/company/works<br/> 
BODY<br/> 
```json
{
	"name": "Coding",
	"emp": 1
}
```
2.2 Получение всех записей из таблицы работ<br/> 
GET <br/> 
localhost:8080/company/works<br/> 
2.3 Получение записи по /id<br/> 
GET <br/> 
localhost:8080/company/works/1<br/> 
2.4. Получение страницы записей из таблицы работ num - номер страницы (> 0), size - кол-во записей на странице (> 0)<br/> 
GET <br/> 
localhost:8080/company/works/page?num=1&size=2<br/> 
