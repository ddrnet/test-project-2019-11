package com.testcase.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.testcase.model.Work;

public interface WorkRepo   extends JpaRepository<Work, Long>{

}
