package com.testcase.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;


import com.testcase.model.Employee;

/*
 * Just for using methods from JpaRepository
 */
public interface EmployeeRepo extends JpaRepository<Employee, Long>, JpaSpecificationExecutor, PagingAndSortingRepository<Employee, Long> {
	
	
	@Query(value = "select * from employees where created_at >= ?1", nativeQuery = true)
	List<Employee> gtCreatedAt(Date date);
	@Query(value = "select * from employees where created_at <= ?1", nativeQuery = true)
	List<Employee> ltCreatedAt(Date date);
	@Query(value = "select * from employees where created_at >= ?1 and created_at <= ?2", nativeQuery = true)
	List<Employee> rangeCreatedAt(Date date1, Date date2);
	
	public List<Employee> findAllByOrderBySalaryDesc();
	public Page<Employee> findAllByOrderBySalaryDesc(Pageable pageble);
	
	public List<Employee> findAllByOrderBySalaryAsc();
	public Page<Employee> findAllByOrderBySalaryAsc(Pageable pageble);

	public List<Employee> findAllByOrderByNameDesc();
	public Page<Employee> findAllByOrderByNameDesc(Pageable pageble);
	
	public List<Employee> findAllByOrderByNameAsc();
	public Page<Employee> findAllByOrderByNameAsc(Pageable pageble);
	
	public List<Employee> findAllByIsWorking(Boolean isWorking);
	public Page<Employee> findAllByIsWorking(Boolean isWorking, Pageable pageble);

	
}
