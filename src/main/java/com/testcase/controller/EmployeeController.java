package com.testcase.controller;


import java.util.Date;
import java.util.List;

import com.testcase.dao.EmployeeDAO;
import com.testcase.dao.WorkDAO;
import com.testcase.model.Employee;
import com.testcase.model.Work;

import javassist.expr.Cast;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/company") 
public class EmployeeController {

    @Autowired
    EmployeeDAO employeeDAO;
    
    /**
     * Добавление строки в таблицу рабочих
     * @param empl добавляемые данные в JSON 
     * @return созданная запись из таблицы (с генерируемыми полями)
     */
    @PostMapping("/employees")
    public Employee createEmployee(@Valid @RequestBody Employee empl) {
        return employeeDAO.save(empl);
    }
    
    /**
     * Получние всех строк таблицы рабочих
     * @return все строки в JSON формате
     */
    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeDAO.findAll();
    }
   
    /**
     * Получение указанной записи из таблицы по id
     * @param id - id рабочего
     * @return строка из таблицы в JSON
     */
    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable(value="id") Long id) {
        Employee empl = employeeDAO.getOne(id);
        if(empl == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok().body(empl);
    }
    @GetMapping("/employees/filter")
	public ResponseEntity<List<Employee>> getEmployeesByFilter( @RequestParam (required = false) String name,
																@RequestParam (required = false) String position,
																@RequestParam (required = false) String salary,
																@RequestParam (required = false)
																@DateTimeFormat(pattern="dd.MM.yyyy")  Date createdAt,
																@RequestParam (required = false) Boolean isWorking) {
		List<Employee> empls = null;
		Employee empl = new Employee();
		if(name  != null)
			empl.setName(name);
		if(position  != null)
			empl.setPosition(position);
		if(salary != null)
			empl.setSalary(Double.valueOf(salary));
		if(createdAt != null)
			empl.setCreatedAt(createdAt);
		if(isWorking != null)
			empl.setIsWorking(isWorking);
		empls = employeeDAO.findFilteredEmployees(empl);
		if(empls == null || empls.size() == 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(empls);
	}

    /**
     * Вывод страницы с данными таблицы рабочих
     * @param page номер страницы 
     * @param size кол-во записей на странице
     * @return указанная страница из таблицы в JSON формате
     */
	@GetMapping("/employees/page")
	public ResponseEntity<List<Employee>> getEmployeesById(@RequestParam Long num, @RequestParam Long size) {
		List<Employee> empls = employeeDAO.findInRange(num-1, size);
		if(empls == null || empls.size() == 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(empls);
	}

    
	/**
	 * 
	 * @param emplId id изменяемого рабочего
	 * @param newEmpl данные нового рабочего
	 * @return ok - если рабочий изменен успешно
	 */
    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployeeById(@PathVariable(value="id") Long emplId, @Valid @RequestBody Employee newEmpl) {
        Employee empl = employeeDAO.getOne(emplId);
        if(empl == null)
            return ResponseEntity.notFound().build();
        empl.setName(newEmpl.getName());
        empl.setPosition(newEmpl.getPosition());
        empl.setCreatedAt(newEmpl.getCreatedAt());
        empl.setIsWorking(newEmpl.getIsWorking());
        Employee updatedEmployee = employeeDAO.save(empl);
        return ResponseEntity.ok().body(updatedEmployee);
    }
    /**
	 * @param emplId id рабочего к удаления
	 * @return возвращает ok если удалено успешно
	 */

    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Employee> deleteEmployeeById(@PathVariable(value="id") Long emplId) {
        Employee empl = employeeDAO.getOne(emplId);
        if(empl == null)
            return ResponseEntity.notFound().build();
        employeeDAO.deleteById(emplId);
        
        return ResponseEntity.ok().build(); 
        
    }
    
    /**
     * @param type указание поля сортировки (имя или зп)
	 * @param direction сортировать по возрастанию(asc), по убыванию(desc).
	 * @param num номер страницы.
	 * @param size кол-во.
	 * @return выводит сотрудников сортированных по указанному полю.
	 */
	@GetMapping("/employees/sort") 
	public ResponseEntity<List<Employee>> getEmployeeSortedBySalary(@RequestParam String type,
																	@RequestParam String direction,
																	@RequestParam Integer num,
																	@RequestParam Integer size) {
		List<Employee> empls = null;
		if(type.toLowerCase().equals("salary"))
		{
			if(direction.toLowerCase().equals("asc")) {
				empls = employeeDAO.sortBySalaryAsc(num - 1,size);
			}
			else if(direction.toLowerCase().equals("desc")) {
				empls = employeeDAO.sortBySalaryDesc(num - 1,size);
			}
		}
		else if(type.toLowerCase().equals("name"))
		{
			if(direction.toLowerCase().equals("asc")) {
				empls = employeeDAO.sortByNameAsc(num - 1,size);
			}
			else if(direction.toLowerCase().equals("desc")) {
				empls = employeeDAO.sortByNameDesc(num - 1,size);
			}
		}
		if (empls == null || empls.size() == 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(empls);
	}
	/**
	 * Формат даты dd.MM.yyyy.
	 * Если задана только начальная дата, то идет поиск значений которые больше этой даты.
	 * Если задана только конечная дата, то идет поиск которые меньше этой даты.
	 * Если заданы оба значения, то поиск осуществляется между ними. 
	 * @param start начальная дата
	 * @param end конечная дата
	 * @return ответ на запрос.
	 */
	@GetMapping("/employees/date")
	public ResponseEntity<List<Employee>> getEmployeesByDate(@RequestParam (required = false)
															  @DateTimeFormat(pattern="dd.MM.yyyy") Date start,
															  @RequestParam (required = false)
															  @DateTimeFormat(pattern="dd.MM.yyyy") Date end) {
		List<Employee> empls = null;
		if(start != null) empls = employeeDAO.findGTDate(start);
		else if(end != null) empls = employeeDAO.findLTDate(end);
		else empls = employeeDAO.findBetween(start, end);
		if(empls == null || empls.size() == 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(empls);
	}

    
}
