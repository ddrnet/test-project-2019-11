package com.testcase.controller;


import java.util.List;

import com.testcase.dao.WorkDAO;
import com.testcase.model.Work;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/company") 
public class WorkController {
	@Autowired
    WorkDAO workDAO;
	/**
	 * Создание записи в таблице работы
	 * @param empl данные о работе в JSON
	 * @return созданная запись из таблицы (с генерируемыми полями)
	 */
    @PostMapping("/works")
    public Work createWork(@Valid @RequestBody Work empl) {
        return workDAO.save(empl);
    }
    /**
     * Получение всех записей из таблицы
     * @return все строки в JSON
     */
    @GetMapping("/works")
    public List<Work> getAllWorks() {
        return workDAO.findAll();
    }
    /**
     * Получение страницы строк
     * @param page номер страницы
     * @param size кол-во строк на странице
     * @return страница в JSON
     */
    @GetMapping("/works/page")
	public ResponseEntity<List<Work>> getWorksById(@RequestParam Long num,@RequestParam Long size ) {
		List<Work> empls = workDAO.findInRange(num - 1, size);
		if(empls == null || empls.size() == 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(empls);
	}
    /**
     * Удаление работы по Id
     * @param emplId id работы для удаления
     * @return ок если удалил 
     */
    @DeleteMapping("/works/{id}")
    public ResponseEntity<Work> deleteWorkById(@PathVariable(value="id") Long emplId) {
        Work empl = workDAO.getOne(emplId);
        if(empl == null)
            return ResponseEntity.notFound().build();
        workDAO.deleteById(emplId);
        
        return ResponseEntity.ok().build();
        
    }
}
