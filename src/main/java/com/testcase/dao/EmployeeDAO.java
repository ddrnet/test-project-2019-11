package com.testcase.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.testcase.model.Employee;
import com.testcase.repository.EmployeeRepo;
import com.testcase.spec.EmpSpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


/*
 *DAO - data access object 
 *Here we will insert/select etc. data in/from db */

@Service 
public class EmployeeDAO {
	
	@Autowired
	EmployeeRepo employeeRepository;
	
	/*save an employee*/
	public Employee save(Employee empl) {
		return employeeRepository.save(empl);
	}

	/*search all employees*/
	public List<Employee> findAll() {
		return employeeRepository.findAll();
	}

	public List<Employee> findInRange(Long emplId1, Long emplId2) { 
		
		return employeeRepository.findAll(PageRequest.of(Math.toIntExact(emplId1), Math.toIntExact(emplId2))).toList();
	}
	/*get an employee by id*/
	public Employee getOne(Long emplId) {
		return employeeRepository.findById(emplId).orElse(null);
	}

	/*delete an employee by Id*/
	public void deleteById(Long emplId) {
		employeeRepository.deleteById(emplId);
	}

	/*delete an employee*/
	public void delete(Employee empl) {
		employeeRepository.delete(empl);
	}
	/*Using specialization*/
	public List<Employee> findFilteredEmployees(Employee empl) {
		try {
			EmpSpec spec = new EmpSpec(empl);
			return employeeRepository.findAll(spec);
		}
		catch(Exception ignored){
			return null;
		}
	}
	public List<Employee> sortBySalaryDesc(Integer page, Integer size) {
		PageRequest pageble = PageRequest.of(Math.toIntExact(page), Math.toIntExact(size));
		return employeeRepository.findAllByOrderBySalaryDesc(pageble).toList();
	}
	public List<Employee> sortBySalaryAsc(Integer page, Integer size) {
		PageRequest pageble = PageRequest.of(Math.toIntExact(page), Math.toIntExact(size));
		return employeeRepository.findAllByOrderBySalaryAsc(pageble).toList();
	}
	public List<Employee> sortByNameDesc(Integer page, Integer size) {
		PageRequest pageble = PageRequest.of(Math.toIntExact(page), Math.toIntExact(size));
		return employeeRepository.findAllByOrderByNameDesc(pageble).toList();
	}
	public List<Employee> sortByNameAsc(Integer page, Integer size) {
		PageRequest pageble = PageRequest.of(Math.toIntExact(page), Math.toIntExact(size));
		return employeeRepository.findAllByOrderByNameAsc(pageble).toList();
	}
	public List<Employee> findGTDate(Date date) {
		try {
			return employeeRepository.gtCreatedAt(date);
		}
		catch(Exception ignored){
			return null;
		}
	}
	public List<Employee> findLTDate(Date date) {
		try {
			return employeeRepository.ltCreatedAt(date);
		}
		catch(Exception ignored){
			return null;
		}
	}
	public List<Employee> findBetween(Date start, Date end) {
		try {
			return employeeRepository.rangeCreatedAt(start, end);
		}
		catch(Exception ignored){
			return null;
		}
	}


}
