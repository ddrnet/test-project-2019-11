package com.testcase.dao;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.testcase.model.Employee;
import com.testcase.model.Work;
import com.testcase.repository.EmployeeRepo;
import com.testcase.repository.WorkRepo;
import com.testcase.spec.EmpSpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


/*
 *DAO - data access object 
 *Here we will insert/select etc. data in/from db */

@Service 
public class WorkDAO {
	@Autowired
	WorkRepo workRepository;
	/*save an work*/
	public Work save(Work empl) {
		return workRepository.save(empl);
	}

	/*search all Work*/
	public List<Work> findAll() {
		return workRepository.findAll();
	}

	public List<Work> findInRange(Long emplId1, Long emplId2) { 
		
		return workRepository.findAll(PageRequest.of(Math.toIntExact(emplId1), Math.toIntExact(emplId2))).toList();
	}
	/*get an Work by id*/
	public Work getOne(Long emplId) {
		return workRepository.findById(emplId).orElse(null);
	}

	/*delete an Work by Id*/
	public void deleteById(Long emplId) {
		workRepository.deleteById(emplId);
	}

	/*delete an Work*/
	public void delete(Work empl) {
		workRepository.delete(empl);
	}
}
