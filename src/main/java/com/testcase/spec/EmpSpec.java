package com.testcase.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import com.testcase.model.Employee;
import com.testcase.model.Employee_;

public class EmpSpec implements Specification<Employee> {
	
	private final Employee example;
	
	public EmpSpec(Employee empl) {
		this.example = empl;
	}
	@Override
	public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();
	    if (StringUtils.isNotBlank(example.getName())) {
	    	predicates.add(cb.like(cb.lower(root.get(Employee_.name)), example.getName().toLowerCase() + "%"));
	    }
	    if (StringUtils.isNotBlank(example.getPosition())) {
	    	predicates.add(cb.like(cb.lower(root.get(Employee_.position)), example.getPosition().toLowerCase() + "%"));
	    }
	    if (example.getIsWorking() != null) {
	        predicates.add(cb.equal(root.get(Employee_.isWorking), example.getIsWorking()));
	    }
	    return andTogether(predicates, cb);
	}
	private Predicate andTogether(List<Predicate> predicates, CriteriaBuilder cb) {
	    return cb.and(predicates.toArray(new Predicate[0]));
	}
}
