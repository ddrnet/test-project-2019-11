package com.testcase.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.type.TrueFalseType;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name="Works")
@EntityListeners(AuditingEntityListener.class)
public class Work {
	 	@Id
	    @GeneratedValue(strategy = GenerationType.AUTO)  //Auto increment when new value inserted
	    private Long id;

		@NotBlank
	    private String name;
		
		@Column(nullable = false)
		private Long Emp;
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		public Long getEmp() {
			return Emp;
		}

		public void setEmp(Long Emp) {
			this.Emp = Emp;
		}


}
