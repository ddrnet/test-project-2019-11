package com.testcase.model;

import java.util.Date;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.type.TrueFalseType;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name="Employees")
@EntityListeners(AuditingEntityListener.class)

//our model
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)  //Auto increment when new value inserted
    private Long id;

	@NotBlank
    private String name;

    @NotBlank
    private String position;

  
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date createdAt;

	@Column(columnDefinition = "BOOLEAN DEFAULT true", nullable = false)
    private Boolean isWorking = true;
	
	@Column(nullable = false)
    private Double salary;
/*
	 
	    private Set<Work> wrk = new HashSet<Work>();

	    @OneToMany(mappedBy = "contact", cascade = CascadeType.ALL, orphanRemoval = true)
	    public Set<Work> getWork() {
	        return this.wrk;
	    }

	    public void setWork(Set<Work> wrk) {
	        this.wrk = wrk;
	    }

	    public void addWork(Work wrk) {
	        wrk.setEmp(this);
	        getWork().add(wrk);
	    }

	    public void removeContactTelDetail(Work wrk) {
	        getWork().remove(wrk);
	    }
	    */
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getIsWorking() {
		return isWorking;
	}

	public void setIsWorking(Boolean isWorking) {
		this.isWorking = isWorking;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public double getSalary() {
		
		return this.salary;
	}


}
